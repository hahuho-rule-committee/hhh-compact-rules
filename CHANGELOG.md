# 1.2
## EN
- Layout
  + Got rid of the redundant 0 in the rule numbering. E.g. rule 1.0a now becomes 1a, but rule 3.1a still is 3.1a.
  + Add indent to easier discern between rules.
- Rulechanges
  + Rule 2b: A team is now only required to have at least one player.
  + Rule 4a2: The _circle_ is now half the size. The length of the shoe is no longer the radius but the diameter.
  + Rule 5a2: Penalty drinks can now be delegated to exactly one remaining player.
  + Rule 7b: Clarified that containers must be held over the head upside down.
- Language
  + Rule 5a3: a ^s^ -> an ^s^
  + Rule 8.1a: hand(s) -> arm(s)

## DE
- Formatänderungen
  + Regelnummerierungen haben nun keine redundante 0 d.h. Regel 1.0a ist jetzt 1a, Regel 3.1a bleibt aber 3.1a.
  + Kleines Indent damit Regelzeilen leichter gefunden werden.
- Regeländerungen
  + Regel 2b: Ein Team muss jetzt nur noch mindestens einE SpielerIn haben.
  + Regel 4a2: Der _Kreis_ ist nun halb so groß, die Schuhlänge entspricht nicht mehr dem Radius sondern dem Durchmesser.
  + Regel 5a2: Strafgetränke können jeweils nun an genau einE MitspielerIn deligiert werden.
  + Regel 7b: Klargestellt, dass das Gefäß verkehrt herum über den Kopf gehalten werden muss.
- Sprachliche Änderungen
  + Regel 5a3: a ^s^ -> an ^s^
  + Regel 8.1a: hand(s) -> arm(s)
