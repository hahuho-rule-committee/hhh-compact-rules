# VERSION 1.2 Draft
all: english german

english: build/en/rules.pdf
german: build/de/rules.pdf

build/:
	mkdir -p $@

build/en/: | build/
	mkdir -p $@

build/de/: | build/
	mkdir -p $@

build/en/version.tex: | build/en/
	$(file >$@,$(shell git tag | grep -E '^v?([0-9]+)\.([0-9]+)-(Draft|Final)\+([0-9]+)$$' | sort --unique | tail -n 1 | sed -E 's#^([0-9]+)\.([0-9]+)-(Draft|Final)\+([0-9]+)$$#English \1.\2 \3#'))

build/de/version.tex: | build/de/
	$(file >$@,$(shell git tag | grep -E '^v?([0-9]+)\.([0-9]+)-(Draft|Final)\+([0-9]+)$$' | sort --unique | tail -n 1 | sed -E 's#^([0-9]+)\.([0-9]+)-(Draft|Final)\+([0-9]+)$$#Deutsch \1.\2 \3#' | sed -E 's#Draft#Vorläufiger Satz#' | sed -E 's#Final#Endgültiger Satz#'))

build/en/date.tex: | build/en/
	$(file >$@,$(shell date --date $(shell git log -1 --format=%ai HEAD) +"%e %B %Y"))

build/de/date.tex: | build/en/

build/%/rules.tex: %/rules.md | build/%/
	pandoc --from markdown --to latex $< --output $@

build/%/rules.pdf: %/main.tex common.tex build/%/rules.tex build/%/version.tex | build/%/
	latexmk -pdf -jobname=$(basename $(notdir $@)) -output-directory=$(dir $@) $<

clean:
	$(RM) -r build/

.PHONY: all clean german english
