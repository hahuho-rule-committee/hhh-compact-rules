
# Disambiguation

#. A _match_ is a one whole game of Flunkyball.
1. A _lap_ is a throw from every player still standing.
1. A _decider_ is a minigame that results in a winner and a loser.
1. A _pitch_ is the area where the _match_ is played.

# Team

#. Flunkyball is played with two teams.
1. A team consists of at least one player.
1. The teams may only differ one in size.

# Items

## Drinks

#. Each player needs at least one container holding a drink.
1. The contents of the container are to be agreed upon the teams.
1. No agreement results in a _decider_, the winner choosing their own drinks,
   the loser may choose the same, or drinks, which in total have at least the
   volume and have at least the same relative alcohol content.

## Target

#. Any object that can be tipped over, however, does not tip over by itself, may
   be the target, agreed upon the teams.
1. No agreement results in a _decider_, the winner choosing the target.

## Throwing Object

#. Any object can be the throwing object, however it must be agreed upon in
advance.
1. Should the object be damaged beyond repair and no longer able to tip over the
   target reliably it can be exchanged for a newly agreed upon object.
1. No agreement results in a _decider_, the winner choosing the throwing object.

# Pitch

#. The _pitch_ consists of
   #. The _centre_
   1. The _circle_, around the _centre_ with the length of the shortest shoe of
      all players as diameter.
   1. The _beerlines_, which are equidistant from the centre, while parallel to
      each other.
1. The _pitch_ has to be agreed upon both teams.
1. No agreement results in a _decider_, the winner choosing the _centre_ and
   _circle_ and the loser choosing the _beerlines_.

# Penalties

#. There are three levels of rule violations, and three corresponding penalties:
   #. Minor -- In the next _Throwing Phase_ of the team of the offending party,
      they may not attempt to throw and the _Throwing Phase_ ends. Rules broken
      which are _Minor_ violations are marked with an ^i^.
   1. Major -- The offending party must take an additional drink. The drink can
      be delegated to exactly one member of the team still remaining. Rules
      broken which are _Major_ violations are marked with an ^a^.
   1. Severe -- The offending party's team may no longer step over their _beerline_ to
      _Restore the Pitch_. _Severe_ violations are marked with an ^s^.
1. At the offending player's discretion it is allowed to choose a higher penalty.
1. Should a penalty no longer be possible, the next higher penalty is given.

# Play

#. A team is either _Offence_ or _Defence_; If a team is _Offence_ the other is
   necessarily the _Defence_ and vice versa.
1. The game consists of three phases, which overlap:
   #. Throwing Phase (_Offence_)
   1. Restore Phase (_Defence_)
   1. Drink Phase (_Offence_)
1. A _decider_ is played, the winner determines if they want to start or choose
   side, the loser may choose side if they winner chose to start.
1. Per default each team has to stand behind the _beerline_.

# Objective

#. The objective of the game is for the own team to finish before the other
team.
1. A player is allowed finish at any time if their team is _Offence_ by proving
   that their drinking container is (quasi) empty, by holding the container
   upside down over their head.

# Phases

## Throwing Phase (YEET IT)

#. ^i^One player of the _Offence_ throws the throwing object, such that at the time of release the
   throwing object is above the elbow(s) of the players throwing arm(s).
1. Upon releasing the throwing object the _Defence_ is allowed to step over
   the _beerlines_.
1. If _throwing object_ hits the target and it tips over the _Offence_ immediately
   transitions into the _Drink Phase_.
1. ^a^After the throwing object is thrown, both teams prohibited from touching
   their drink until this phase ends.

## Restore Phase (SKEET IT)

#. If the _Offence_ successfully tipped over the target during the throwing phase
   the drink phase endures until the _Defence_ repositioned the _target_ within
   the _circle_, and all the players of the _Defence_ are behind their
   _beerline_, this action is called _Restoring the Pitch_.
1. ^i^After _Restoring the Pitch_ the _Defence_ must signal the _Offence_ that the
   _Drink Phase_ ended.
1. After _Restoring the Pitch_ touching their own drink is allowed again.

## Drink Phase (DELETE IT)

#. ^i^The _Offence_ may touch their own drinks and start drinking until the
   _Defence_ successfully _Restored the Pitch_ and they are signalled that the
   _Drink Phase_ is over.

# Punishable offences

#. ^i^Leaving the pitch, without unanimous vote of all remaining players.
1. ^i^Touching someone else's drink.
1. ^i^The container tipping over without spilling drinks. In this case one is
   allowed to touch their drink.
1. ^a^Touching one's own drink, when not explicitly allowed.
1. ^a^Spitting out drinks.
1. ^a^Puking.
1. ^s^Deliberately spilling drinks.

# Tie Breaker

#. If both teams do not hit the target for two _laps_, both teams are allowed to
move their _beerline_ two times the length of the shortest shoe of all players still in the
game, towards the _centre_.
