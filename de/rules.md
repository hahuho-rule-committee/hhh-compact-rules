
# Spielvorbereitung

## Begriffserklärung

#. Ein _Match_ ist ein durchgängiges Spiel Flunkyball.
1. Ein _Wurf_ ist von einer SpielerIn getätigter Wurf.
1. Eine _Runde_ ist ein _Wurf_ von jedem Team.
1. Eine _Etappe_ ist ein _Wurf_ von alljenen die noch im Spiel sind.
1. Ein _Entscheidungsspiel_ ist ein Minispiel, dass ein Gewinnerteam und Verliererteam
   hat.
1. Ein _Spielfeld_ ist der Ort an dem ein _Match_ ausgetragen wird.

## Team

#. Zwei Teams spielen gegeneinander Flunkyball.
1. Ein Team besteht aus zumindest zwei SpielerInnen.
1. Die Anzahl der SpielerInnen dürfen sich bei den Teams nur um eins unterscheiden.

## Gegenstände

### Getränke

#. Alle SpielerInnen brauchen jeweils ein eigenes Getränkebehältnis.
1. Der Inhalt des Getränkebehältnis muss im Vorhinein von den Teams festgelegt werden.
1. Kommt es zu keiner Einigung so wird ein _Entscheidungsspiel_ ausgetragen, das
   Gewinnerteam wählt die eigenen Getränke, das Verliererteam darf entweder die
   gleichen Getränke, oder Getränke die in der Summe zumindest das gleiche
   Volumen und den selben relativen Alkoholgehalt haben.

### Wurfziel

#. Jegliches Objekt welches umfallen kann, dies jedoch nicht von selbst tut, kann
   das Wurfziel sein. Das Wurfziel muss im Vorhinein von den Teams festgelegt werden.
1. Kommt es zu keiner Einigung so wird ein _Entscheidungsspiel_ ausgetragen, das
   Gewinnerteam wählt das Wurfziel.

### Wurfobjekt

#. Jegliches Objekt kann das Wurfobjekt sein. Das Wurfobjekt muss im Vorhinein
   von den Teams festgelegt werden.
1. Sollte das Objekt irreparabel beschädigt sein, sodass es nicht mehr in der
   Lage ist das Wurfziel zuverlässig umfallen zu lassen, so kann es nach
   Einigung der Teams für ein neues Objekt umgetauscht werden.
1. Kommt es zu keiner Einigung so wird ein _Entscheidungsspiel_ ausgetragen, das
   Gewinnerteam wählt das Wurfobjekt.

## Spielfeld

#. Das _Spielfeld_ besteht aus
   #. Der _Mitte_
   1. Den _Bierlinienmarkierungen_, welche im gleichen Abastand von der _Mitte_,
      aber parallel zueinander stehen.
1. Beide Teams müssen mit dem _Spielfeld_ einverstanden sein.
1. Kommt es zu keiner Einigung so wird ein _Entscheidungsspiel_ ausgetragen, das
   Gewinnerteam wählt die _Mitte_, das Verliererteam die _Bierlinienmarkierungen_.

## Strafen

#. Es gibt drei Schweregrade von Regelverstößen und drei zugehörige Strafen:
   #. Leicht -- In der nächsten _Wurfphase_ dürfen die _Angreifer_ das
      Wurfobjekt nicht werfen und die _Wurfphase_ endet sofort. Regeln die einen
      _Leichten_ Regelverstoß zugehören sind mit einem ^L^ markiert.
   1. Schwer -- Die fehlbaren SpielerInnen müssen sich ein weiteres Getränk
      nehmen, das alte trägt keinen Einfluss mehr auf das Spiel. Regeln die einen
      _Schweren_ Regelverstoß zugehören sind mit einem ^S^ markiert.
   1. Heftig -- Das Team der fehlbaren SpielerInnen haben verloren. Regeln die einen
      _Heftigen_ Regelverstoß zugehören sind mit einem ^H^ markiert.
1. Auf Wunsch der fehlbaren SpielerIn, darf auch eine höheres Strafmaß angelegt werden.
1. Sollte es nicht mehr möglich sein eine Strafe zu erteilen so gilt die nächst
   höhere Strafe.

# Spielablauf

#. Teams sind entweder _Angreifer_ oder _Verteidiger_. Ist ein Team _Angreifer_
   so ist das andere _Verteidiger_ und vice versa.
1. Das Spiel hat drei sich wiederholende und überlappende Spielphasen:
   #. Wurfphase (_Angreifer_)
   1. Rückstellphase (_Verteidiger_)
   1. Trinkphase (_Angreifer_)
1. Ein _Entscheidungsspiel_ wird ausgetragen, das _Gewinnerteam_ bestimmt ob es
   beginnt oder von welcher Seite es spielt, das Verliererteam wählt die Seiten
   wenn das Gewinnerteam beginnt.
1. Grundsätzlich stehen die Teams hinter ihrer _Biermarkierungslinie_.

## Spielziel

#. Das Ziel des Spiels ist es vor dem anderen Team, fertig zu werden.
1. Fertig werden kann von den SpielerInnen während ihr Team die _Angreifer_ sind
   angezeigt werden, indem sie beweisen, dass ihr Getränksbehältnis leer ist.
   Dazu halten die SpielerInnen es verkehrt über ihren Kopf.

## Phases

### Throwing Phase (YEET IT)

#. ^i^One player of the offence throws the throwing object, such that at the time of release the
   throwing object is above the elbow(s) of the players throwing hand(s).
1. If throwing object hits the target and it tips over the offence immediately
   transitions into the _Drink Phase_.
1. ^i^The defence is allowed to defend their drinks by putting one foot in front of
   their drinks.
1. ^a^After the throwing object is thrown, both teams prohibited from touching
   their drink until this phase ends.

### Restore Phase (SKEET IT)

#. If the offence successfully tipped over the target during the throwing phase
   the drink phase endures until the defence repositioned the target into its
   original position, and all the players of the defence are behind their
   _beerline_, this action is called _Restoring the Pitch_.
1. ^i^After _Restoring the Pitch_ the defence must signal the offence that the
   _Drink Phase_ ended.
1. After _Restoring the Pitch_ touching their own drink is allowed again.

### Drink Phase (DELETE IT)

#. ^i^The offence may touch their own drinks and start drinking until the defence
successfully _Restored the Pitch_ and they are signalled that the _Drink Phase_
is over.

## Punishable Offences

#. ^i^Leaving the pitch, without unanimous vote.
1. ^i^Touching someone else's drink.
1. ^a^Touching one's own drink, while not in _Drink Phase_, or after _Restoring
   the Pitch_.
1. ^a^Spitting out drinks.
1. ^a^Puking.
1. ^s^Deliberately spilling drinks.

## Tie Breaker

#. If both teams do not hit the target for two _laps_, both teams are allowed to
move their _beerline_ the width of the smallest container still in the game,
towards the _centre_.
